package com.example.weatherapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WeatherViewModel: ViewModel() {
    val apiManager = MutableLiveData<APIManager>()
    val savedCityList = MutableLiveData<Array<City>>() //List of cities from database
    val currentCity = MutableLiveData<City>() //Current search result

    init {
        apiManager.value = APIManager(this)
        savedCityList.value = emptyArray()
    }

    fun setCurrentCity(city: City) {
        currentCity.value = city
        currentCity.postValue(city)
    }
}