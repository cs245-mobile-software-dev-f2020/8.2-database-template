package com.example.weatherapp

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

class APIManager(val weatherViewModel: WeatherViewModel) { //2
    private val apiURL = "https://api.openweathermap.org/"
    private val apiKey = "79095b87aa25b99305df0c7df90d4772"
    private val units = "imperial"

    val retrofit = Retrofit.Builder()
        .baseUrl(apiURL)
        .build()

    val service = retrofit.create(WeatherService::class.java)

    interface WeatherService {
        @GET("data/2.5/weather?")
        fun getWeatherByCity(
            @Query("q") cityName: String,
            @Query("appid") appid: String,
            @Query("units") units: String
        ): Call<ResponseBody>
    }

    fun decodeJson(json: String) {
        val city = City()
        val data = JSONObject(json)
        city.name = data.getString("name")
        val main = data.getJSONObject("main")
        city.temperature = main.getDouble("temp")
        city.pressure = main.getInt("pressure")
        city.humidity = main.getInt("humidity")
        weatherViewModel.setCurrentCity(city)
    }

    fun fetchWeather(cityName: String) {
        val call = service.getWeatherByCity(cityName, apiKey, units)
        call.enqueue(WeatherCallback())
    }

    inner class WeatherCallback() :
        Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        }

        override fun onResponse(
            call: Call<ResponseBody>,
            response: Response<ResponseBody>
        ) {
            if (response.isSuccessful) {
                response.body()?.let {
                    decodeJson(it.string())
                }
            }
        }
    }

}