package com.example.weatherapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_weather.*

/**
 * A simple [Fragment] subclass.
 * Use the [WeatherFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeatherFragment : Fragment() {

    val viewModel: WeatherViewModel by activityViewModels<WeatherViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        search_button.setOnClickListener {
            if (name_text.text.isNotEmpty()) {
                viewModel.apiManager.value?.fetchWeather(name_text.text.toString())
            }
        }

        save_button.setOnClickListener {
            findNavController().navigate(R.id.action_global_listFragment)
        }

        cancel_button.setOnClickListener {
            findNavController().navigate(R.id.action_global_listFragment)
        }

        viewModel.currentCity.observe(viewLifecycleOwner, {
            city_text.text = it.name
            temp_text.text = "${it.temperature} ℉"
            hum_text.text = "${it.humidity}%"
            press_text.text = it.pressure.toString()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }
}